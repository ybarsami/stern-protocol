Pour tester le programme, nous avons besoin de deux machines qui peuvent communiquer entre elles (via leur adresse IP), et qui peuvent compiler le code source.

1. Tout d'abord, compiler sur les deux machines (problème de compilation avec gcc-11 apparemment). Cela va créer deux exécutables "prover" et "verifier" :

    make

2. Ensuite, récupérer "IP_prouveur", l'IP de la machine du prouveur. Pour cela, sur la machine du prouveur, aller par ex. sur https://www.whatismyip.com ou bien taper en ligne de commande :

    hostname -I | awk '{print $1}'

3. Ensuite, récupérer "IP_vérifieur", l'IP de la machine du vérifieur. Pour cela, faire de même qu'au point précédent.

4. Il faut ensuite que la machine du prouveur autorise la machine du vérifieur à se connecter. Pour cela, sur la machine du prouveur, activer les entrées depuis l'IP du vérifieur :

    sudo ufw allow from IP_vérifieur

5. Sur la machine du prouveur, lancer le programme en choisissant un port (12345 dans l'exemple) et une valeur pour N (optionnelle, par défaut c'est 1024)

    ./prover 12345 2048

6. Sur la machine du vérifieur, lancer le programme en fournissant l'IP du prouveur, le port qu'il a ouvert, et une valeur pour N (optionnelle, par défaut c'est 1024)

    ./verifier IP_prouveur 12345 2048

6.bis. Pour sauvegarder les logs :

    ./verifier IP_prouveur 12345 2048 | tee PStrasbourg_VSaintry_2048.log
    ./verifier IP_prouveur 12345 2048 > PStrasbourg_VSaintry_2048.log

