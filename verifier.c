/*
 * gcc random.c verifier.c -o verifier -lgmp
 */

#include <arpa/inet.h>   // functions htons (host-to-network short)
#include <assert.h>      // macro     assert
#include <math.h>        // functions ceil, log2 (logarithm base 2)
#include <netinet/in.h>  // type      struct sockaddr_in
#include <netdb.h>       // type      struct hostent
                         // function  gethostbyname
#include <stdint.h>      // types     int64_t, uint32_t, int32_t
#include <stdio.h>       // functions fprintf, perror, sprintf
#include <gmp.h>         // WARNING: include stdio.h before gmp.h if we use functions mpz_out_raw, mpz_inp_raw
                         // type      mpz_t
                         // functions mpz_init(s), mpz_clear(s)
                         // functions mpz_add, mpz_mod, mpz_mul, mpz_cmp, mpz_sizeinbase
#include <stdlib.h>      // functions exit (error handling), atoi (convert to integer)
                         // constant  EXIT_FAILURE (error handling)
                         // type      size_t
#include <string.h>      // functions memcpy, memset
#include <sys/types.h>
#include <sys/socket.h>  // functions connect (counterpart of server's accept), socket
                         // constants AF_INET, SOCK_STREAM
#include <sys/time.h>    // function  gettimeofday
#include <time.h>        // type      struct tm, struct timespec
                         // functions gmtime, nanosleep
#include <unistd.h>      // functions close, read, write
#include "maths.h"       // functions ceiling_division, handle_mpz_import, handle_mpz_export, mersenne_prime
#include "matrices.h"    // functions allocate_char_matrix, deallocate_char_matrix
#include "parameters.h"  // constants N, K, W, NB_TESTS
#include "random.h"      // macros    stern_seed_RNG, stern_free_RNG
                         // functions stern_random_bit_matrix, stern_random_bit_vector, stern_random_weighted_bit_vector
                         //           stern_random_permutation, stern_next_random_bytes, seed_64bits

void error(const char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    // Client-server connection.
    int sockfd, portno;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    ssize_t bytes_exchanged;
    // Timing.
    struct timeval timeval_before;
    struct tm* utc_tm_before;
    struct timeval timeval_after;
    struct tm* utc_tm_after;
    
    // Usage of client requires hostname (www.whatismyip.com or hostname -I | awk '{print $1}')
    // and port number (between 2000 and 65535, chosen by the server).
    // Value for N is optional.
    if (argc < 3) {
        fprintf(stderr, "Usage: %s hostname port [N]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR: no such host\n");
        exit(EXIT_FAILURE);
    }
    portno = atoi(argv[2]);
    if (portno < 2000 || portno > 65535) {
        fprintf(stderr, "ERROR: port must be between 2000 and 65535\n");
        exit(EXIT_FAILURE);
    }
    // Default value for N: see parameters.h
    int N = (argc < 4) ? DEFAULT_N : atoi(argv[3]);
    if (N <= 0) {
        fprintf(stderr, "ERROR: N must be greater than 0\n");
        exit(EXIT_FAILURE);
    }
    int K = ceiling_division(N, 2);
    int W = (int)ceil(0.11 * N);
    
    int nb_indices = 3;
    // All the computations are done thanks to the gmp library which works with the following data type:
    mpz_t a[nb_indices], b[nb_indices], z[nb_indices], y[nb_indices], q[nb_indices];
    for (size_t i = 0; i < nb_indices; i++) {
        mpz_inits(a[i], b[i], z[i], y[i], q[i], (void*)0);
    }
    // q[0], q[1], q[2] depend on the security we want to achieve --- the parameter N
    // 64 is chosen so that (2^{-64})^{1/4} << 1 (soundness loss)
    mersenne_prime(N * ceil(log2(N)) + 2 * N + 64, q[0]); // q[0] := 2^p[0] - 1
    mersenne_prime(N * ceil(log2(N)) + 2 * N + 64, q[1]); // q[1] := 2^p[1] - 1
    mersenne_prime(N * ceil(log2(N)) + 2 * N + 64, q[2]); // q[2] := 2^p[2] - 1
    size_t q_size[nb_indices]; // Size in bytes
    for (size_t i = 0; i < nb_indices; i++) {
        q_size[i] = ceiling_division(mpz_sizeinbase(q[i], 2), 8);
    }
    size_t prefix_sum_of_prime_sizes[nb_indices + 1];
    prefix_sum_of_prime_sizes[0] = 0;
    for (size_t i = 0; i < nb_indices; i++) {
        prefix_sum_of_prime_sizes[i + 1] = prefix_sum_of_prime_sizes[i] + q_size[i];
    }
    size_t sum_of_prime_sizes = prefix_sum_of_prime_sizes[nb_indices];
    // As input, Prover1, Prover2, Verifier1, and Verifier 2 all know:
    //     * a matrix H in {0, 1}^(n-k)*n
    char** H = allocate_char_matrix(N - K, N);
    stern_random_bit_matrix(N - K, N, H);
    // TODO: have only one participant compute H and send it to the others.
    //     * a column vector s in {0, 1}^(n-k) --- the syndrome
    char* s = malloc((N - K) * sizeof(char));
    // Prover1 and Prover2 share:
    //     * a column vector e in {0, 1}^n s.t. |e| = w and H*e = s
    // REMARK: here, the prover first computes a random vector e then deduces s.
    char* e = malloc(N * sizeof(char));
    stern_random_weighted_bit_vector(N, W, e);
    char_matrix_vector_multiply(N - K, N, H, e, s);
    //     * a random permutation (sigma) on [n], encoded in n*ceil(n) bits as (sigma_encoded)
    int* sigma = malloc(N * sizeof(int));
    mpz_t sigma_encoded;
    mpz_init(sigma_encoded);
    //     * a random column vector (t) on {0, 1}^n
    char* t = malloc(N * sizeof(char));
    //     * three numbers (a[i]) in Z/q[i] Z --- a[i] is stored on q_size[i] chars.
    char* a_char = malloc(sum_of_prime_sizes * sizeof(char));
    // Verifier1 and Verifier2 share:
    //     * three numbers (b[i]) in Z/q[i] Z --- b[i] is stored on q_size[i] chars.
    //       (those numbers are sent to Prover1 at each round)
    char* b_char = malloc(sum_of_prime_sizes * sizeof(char));
    // At each round, Prover1 computes:
    //     * s_prime = H * t
    char* s_prime_char = malloc((N - K) * sizeof(char));
    mpz_t s_prime;
    mpz_init(s_prime);
    //     * z[0] = s_prime + 2^(n-k) * sigma_encoded --- it is in Z/q[0] Z
    //     * z[1] = sigma(t)                          --- it is in Z/q[1] Z
    //     * z[2] = sigma(t xor e)                    --- it is in Z/q[2] Z
    char* z_char = malloc(sum_of_prime_sizes * sizeof(char));
    char* tmp_vector = malloc(N * sizeof(char));
    char* z1_vector = malloc(N * sizeof(char));
    char* z2_vector = malloc(N * sizeof(char));
    // then Prover1 commits to Verifier1 (y[i]) = (a[i]+b[i]*z[i]).
    char* y_char = malloc(sum_of_prime_sizes * sizeof(char));
    // then Verifier2 sends a challenge to Prover2.
    char challenge;
    // then Prover2 sends to Verifier2 a[i] and z[i] for i != challenge.
    char* az_char = malloc(2 * sum_of_prime_sizes * sizeof(char));
    size_t prefix_sum_of_az_sizes[2 * nb_indices + 1];
    prefix_sum_of_az_sizes[0] = 0;
    size_t sum_of_az_sizes;
    
    // Open the port.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR opening socket");
    }
    
    // Get the server information.
    memset((char*) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    memcpy((char*) &serv_addr.sin_addr.s_addr,
           (char*) server->h_addr,
           server->h_length);
    serv_addr.sin_port = htons(portno);
    
    // Connect to Prover1.
    if (connect(sockfd,
                (struct sockaddr*) &serv_addr,
                sizeof(serv_addr)) < 0) {
        error("ERROR connecting");
    }
    
    // Seed the random number generator.
    stern_seed_RNG(seed_64bits(0));
    
    double distance_Paris_Tokyo = 9719087.0;
    double c = 299792458.0;
    printf("distance(Verifier1, Verifier2) = %0.3lfkm (Paris - Tokyo). d/c = %0.3lfms\n",
           distance_Paris_Tokyo / 1000., distance_Paris_Tokyo /c * 1000.);
    
  // TESTING: NB_TESTS iterations of the protocol, to get statistics on timings.
  for (size_t i_test = 0; i_test < NB_TESTS; i_test++) {
    // sleep for 500 ms, because the prover has some computations to make and
    // we don't want to take this computation into account for the timing.
    // https://stackoverflow.com/questions/1157209/is-there-an-alternative-sleep-function-in-c-to-milliseconds
    struct timespec ts;
    int milliseconds = 0;
#ifdef SLEEP
    if (N > 16384) {
        milliseconds = 4600; // 2300 measured for 32768
    } else if (N > 8192) {
        milliseconds = 1200; // 600 measured for 16384
    } else if (N > 4096) {
        milliseconds = 300; // 150 measured for 8192
    } else if (N > 2048) {
        milliseconds = 70; // 35 measured for 4096
    } else {
        milliseconds = 20; // 10 measured for 2048
    }
#endif
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
    
    /*****************************************************************************
     *                         Phase 1 begin: get time T0                        *
     *****************************************************************************/
    gettimeofday(&timeval_before, NULL);
    utc_tm_before = gmtime(&timeval_before.tv_sec);
    
    /*****************************************************************************
     *                     Prepare the random numbers (b)                        *
     *****************************************************************************/
    for (size_t i = 0; i < nb_indices; i++) {
        size_t offset = prefix_sum_of_prime_sizes[i];
        // Generate a random number in [0; q[i]) --- TODO: from shared source
        do {
            stern_next_random_bytes(&b_char[offset], q_size[i]);
            handle_mpz_import(b[i], &b_char[offset], q_size[i]);
            mpz_mod(b[i], b[i], q[i]);
        } while (mpz_cmp(b[i], q[i]) >= 0);
        handle_mpz_export(&b_char[offset], b[i], q_size[i]);
    }
    
    /*****************************************************************************
     *                       Send the random numbers (b)                         *
     *****************************************************************************/
    // Send the number (b) in [0; q[i]).
    bytes_exchanged = 0;
    while (bytes_exchanged != sum_of_prime_sizes) {
        ssize_t bytes_written = write(sockfd,
                                      &b_char[bytes_exchanged],
                                      sum_of_prime_sizes - bytes_exchanged);
        if (bytes_written < 0) {
            error("ERROR writing (b) to socket");
        }
        bytes_exchanged += bytes_written;
    }
    
    /*****************************************************************************
     *                Receive the committed bits (y) = (a+b*z)                   *
     *****************************************************************************/
    // Receive the number (y) in [0; q[i]).
    bytes_exchanged = 0;
    while (bytes_exchanged != sum_of_prime_sizes) {
        ssize_t bytes_read = read(sockfd,
                                  &y_char[bytes_exchanged],
                                  sum_of_prime_sizes - bytes_exchanged);
        if (bytes_read < 0) {
            error("ERROR reading (e) from socket");
        }
        bytes_exchanged += bytes_read;
    }
    for (size_t i = 0; i < nb_indices; i++) {
        size_t offset = prefix_sum_of_prime_sizes[i];
        handle_mpz_import(y[i], &y_char[offset], q_size[i]);
    }
    
    /*****************************************************************************
     *                          Phase 1 end: get time T1                         *
     *****************************************************************************/
    gettimeofday(&timeval_after, NULL);
    utc_tm_after = gmtime(&timeval_after.tv_sec);
    
    // Check that T1-T0 <= distance(Verifier1, Verifier2)/c --- where c is speed of light.
    printf("Phase 1; Size of message = %0.3lfKo; ", 2 * sum_of_prime_sizes / 1000.0);
    printf("Time elapsed = %0.3lfms\n",
           (timeval_after.tv_sec  - timeval_before.tv_sec ) * 1000. +
           (timeval_after.tv_usec - timeval_before.tv_usec) / 1000.);
    
    /*****************************************************************************
     *                         Phase 2 begin: get time T2                        *
     *****************************************************************************/
    gettimeofday(&timeval_before, NULL);
    utc_tm_before = gmtime(&timeval_before.tv_sec);
    
    /*****************************************************************************
     *                   Send the challenge (c) in {0, 1, 2}                     *
     *****************************************************************************/
    challenge = (char)stern_next_random_ranged_int(nb_indices);
    bytes_exchanged = 0;
    while (!bytes_exchanged) {
        ssize_t bytes_written = write(sockfd,
                                      &challenge,
                                      1);
        if (bytes_written < 0) {
            error("ERROR writing (c) to socket");
        }
        bytes_exchanged += bytes_written;
    }
    
    /*****************************************************************************
     *                               Phase 3 begin                               *
     *****************************************************************************/
    // Computation of the positions of the numbers (a) and (z) in the buffer.
    size_t i_az = 0;
    for (size_t i = 0; i < nb_indices; i++) {
        if (i == challenge)
            continue;
        prefix_sum_of_az_sizes[i_az + 1] = prefix_sum_of_az_sizes[i_az] + q_size[i];
        i_az++;
        prefix_sum_of_az_sizes[i_az + 1] = prefix_sum_of_az_sizes[i_az] + q_size[i];
        i_az++;
    }
    size_t sum_of_az_sizes = prefix_sum_of_az_sizes[2 * (nb_indices - 1)];
    
    // Receive the numbers (a) and (z) in [0; q[i]) for i different from challenge.
    bytes_exchanged = 0;
    while (bytes_exchanged != sum_of_az_sizes) {
        ssize_t bytes_read = read(sockfd,
                                  &az_char[bytes_exchanged],
                                  sum_of_az_sizes - bytes_exchanged);
        if (bytes_read < 0) {
            error("ERROR reading (a) and (z) from socket");
        }
        bytes_exchanged += bytes_read;
    }
    
    /*****************************************************************************
     *                          Phase 3 end: get time T3                         *
     *****************************************************************************/
    gettimeofday(&timeval_after, NULL);
    utc_tm_after = gmtime(&timeval_after.tv_sec);
    
    // Check that T3-T2 <= distance(Verifier1, Verifier2)/c --- where c is speed of light.
    printf("Phase 2; Size of message = %0.3lfKo; ", (1 + sum_of_az_sizes) / 1000.0);
    printf("Time elapsed = %0.3lfms\n",
           (timeval_after.tv_sec  - timeval_before.tv_sec ) * 1000. +
           (timeval_after.tv_usec - timeval_before.tv_usec) / 1000.);
    
    /*****************************************************************************
     *                             Phase 4: checking                             *
     *****************************************************************************/
    i_az = 0;
    for (size_t i = 0; i < nb_indices; i++) {
        if (i == challenge)
            continue;
        size_t offset = prefix_sum_of_az_sizes[i_az++];
        handle_mpz_import(a[i], &az_char[offset], q_size[i]);
        offset = prefix_sum_of_az_sizes[i_az++];
        handle_mpz_import(z[i], &az_char[offset], q_size[i]);
        
        // Check consistency.
        // y = a + b * z
        mpz_t expected_result;
        mpz_init(expected_result);
        mpz_mul(expected_result, b[i], z[i]);
        mpz_add(expected_result, expected_result, a[i]);
        mpz_mod(expected_result, expected_result, q[i]);
        int cmp = mpz_cmp(y[i], expected_result);
        mpz_clear(expected_result);
        assert(cmp == 0);
    }
  }
    
    // Be clean (-:
    close(sockfd);
    stern_free_RNG();
    deallocate_char_matrix(H, N - K, N);
    for (size_t i = 0; i < nb_indices; i++) {
        mpz_clears(a[i], b[i], z[i], y[i], q[i], (void*)0);
    }
    mpz_clear(sigma_encoded);
    mpz_clear(s_prime);
    free(s);
    free(e);
    free(sigma);
    free(t);
    free(s_prime_char);
    free(tmp_vector);
    free(z1_vector);
    free(z2_vector);
    free(a_char);
    free(b_char);
    free(z_char);
    free(y_char);
    return 0;
}

