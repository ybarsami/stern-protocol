#ifndef STERN_RANDOM
#define STERN_RANDOM

#include <stdint.h>       // type      int32_t
#include <stdio.h>        // function  fprintf (output strings on a stream)
                          // constant  stderr (standard error output stream)
#include <stdlib.h>       // function  exit (error handling)
                          // constant  EXIT_FAILURE (error handling)
                          // type      size_t
#include <sys/time.h>     // function  gettimeofday
                          // type      struct timeval
#include <time.h>         // function  time
#include <unistd.h>       // functions gethostid (need _BSD_SOURCE .or. _XOPEN_SOURCE .geq. 500)
                          //           getpid
#include "maths.h"        // functions ceiling_division, min

typedef double (*next_random_double)(void);
typedef int32_t (*next_random_int)(void);
typedef void (*seed_RNG_ULL)(unsigned long long int);
typedef void (*seed_RNG_UL )(unsigned long int);
typedef void (*seed_RNG_U  )(unsigned int);
typedef void (*seed_RNG_L  )(long int);
typedef void (*delete_RNG)(void);

/*
 * Hash function that takes 3 unsigned long ints.
 *
 * http://burtleburtle.net/bob/hash/doobs.html
 */
static inline unsigned long int mix(unsigned long int a, unsigned long int b, unsigned long int c) {
    a=a-b;  a=a-c;  a=a^(c >> 13);
    b=b-c;  b=b-a;  b=b^(a << 8);
    c=c-a;  c=c-b;  c=c^(b >> 13);
    a=a-b;  a=a-c;  a=a^(c >> 12);
    b=b-c;  b=b-a;  b=b^(a << 16);
    c=c-a;  c=c-b;  c=c^(b >> 5);
    a=a-b;  a=a-c;  a=a^(c >> 3);
    b=b-c;  b=b-a;  b=b^(a << 10);
    c=c-a;  c=c-b;  c=c^(b >> 15);
    return c;
}

/*
 * Returns a 32-bit seed from time, process ID and host ID.
 * Also uses mpi_rank for the probable case where different MPI processes
 * are launched on the same machine.
 *
 * One could alternatively use bits from /dev/(u)random
 */
static inline unsigned long int seed_32bits(int mpi_rank) {
    unsigned long int time_value = (unsigned long int)time((void*)0);
    unsigned long int process_id = (unsigned long int)getpid();
    unsigned long int host_id    = (unsigned long int)gethostid() + (unsigned long int)mpi_rank;
    return mix(time_value, process_id, host_id);
}

/*
 * Returns a 64-bit seed from time, process ID and host ID.
 * Also uses mpi_rank for the probable case where different MPI processes
 * are launched on the same machine.
 *
 * One could alternatively use bits from /dev/(u)random
 */
static inline unsigned long long int seed_64bits(int mpi_rank) {
    struct timeval tv;
    gettimeofday(&tv, (void*)0);
    unsigned long int time_value1 = (unsigned long int)tv.tv_sec;
    unsigned long int time_value2 = (unsigned long int)tv.tv_usec;
    unsigned long int process_id  = (unsigned long int)getpid();
    unsigned long int host_id     = (unsigned long int)gethostid() + (unsigned long int)mpi_rank;
    unsigned long long int  low_32bits = (unsigned long long int)mix(time_value1, process_id, host_id);
    unsigned long long int high_32bits = (unsigned long long int)mix(time_value2, process_id, host_id);
    return low_32bits | (high_32bits << 32);
}

enum RNG {
    BAD_RAND_RNG,
    BAD_KISS_RNG,
    RAND_RNG,
    RAND48_RNG,
    KISS_RNG,
    MERSENNE_TWISTER_RNG,
    WELL_RNG,
    NB_RNG // Always has to be last if you update this enum !
};

char rng_names[NB_RNG][99];
next_random_double next_random_doubles[NB_RNG];
next_random_int next_random_ints[NB_RNG];
seed_RNG_ULL seed_RNG_ULLs[NB_RNG];
seed_RNG_UL  seed_RNG_ULs [NB_RNG];
seed_RNG_U   seed_RNG_Us  [NB_RNG];
seed_RNG_L   seed_RNG_Ls  [NB_RNG];
delete_RNG delete_RNGs[NB_RNG];


// Change the following line to change the RNG used in the Stern protocol
// (or compile with -DSTERN_CHOSEN_RNG=...).
#if !defined(STERN_CHOSEN_RNG)
#    define STERN_CHOSEN_RNG KISS_RNG
#endif

// If you want to know how many calls were made to random numbers, tou can compile the Stern protocol
// with -DSTERN_NB_RAND_CALLS, allowing you to use the following variable.
#ifdef STERN_NB_RAND_CALLS
    unsigned long long int nb_rand_calls;
#endif

/*
 * Returns a random 64-bit signed double-precision floating-point number.
 *
 * @return a random number.
 */
#define stern_next_random_double next_random_doubles[STERN_CHOSEN_RNG]

/*
 * Returns a random 32-bit signed integer.
 *
 * @return a random number.
 */
#define stern_next_random_int next_random_ints[STERN_CHOSEN_RNG]

/*
 * Fills a byte array with random numbers.
 *
 * @see getrandom in Linux : http://man7.org/linux/man-pages/man2/getrandom.2.html
 *
 * @param[in]  buflen, the number of random bytes to output.
 * @param[out] buf, the char array in which to output the random bytes.
 */
static inline void stern_next_random_bytes(char* buf, size_t buflen) {
    size_t id_char = 0;
    for (size_t id_int = 0; id_int < ceiling_division(buflen, 4); id_int++) {
        // Generate a 32-bit int.
        int32_t random_int = stern_next_random_int();
        for (; id_char < min(id_int * 4, buflen); id_char++) {
            // Split the random 32-bit int as 4 8-bit chars.
            buf[id_char] = (char)(random_int & 255);
            random_int = random_int / 256;
        }
    }
}

/*
 * Returns a random integer in [0; max).
 *
 * @param[in] max, the maximum value (excluded).
 * @return    the random number in [0, max).
 */
static inline unsigned int stern_next_random_ranged_int(unsigned int max) {
    return (stern_next_random_int() % max + max) % max; // % is the sign of the number, so to have in [0, max) instead of (-max, max).
}

/*
 * Returns a random permutation on n numbers.
 *
 * @param[in]  n, the number of numbers.
 * @param[out] sigma, the permutation on n numbers.
 */
static inline void stern_random_permutation(int n, unsigned int* sigma) {
    for (size_t i = 0; i < n; i++)
        sigma[i] = i + 1;
    
    for (size_t i = 0; i < n; i++) {
        unsigned int k = stern_next_random_ranged_int(n - i);
        unsigned int tmp = sigma[k];
        sigma[k] = sigma[n - i - 1];
        sigma[n - i - 1] = tmp;
    }
}

/*
 * Returns a random bit matrix on {0, 1}^nbRow*nbCol.
 *
 * @param[in]  nbRow, nbCol, the size of the matrix.
 * @param[out] matrix, the random matrix.
 */
static inline void stern_random_bit_matrix(int nbRow, int nbCol, char** matrix) {
    for (size_t i = 0; i < nbRow; i++) {
        for (size_t j = 0; j < nbCol; j++) {
            matrix[i][j] = stern_next_random_ranged_int(2);
        }
    }
}

/*
 * Returns a random bit vector on {0, 1}^n of Hamming weight w.
 *
 * @param[in]  n, the size of the vector.
 * @param[in]  w, the wanted Hamming weight of the vector.
 * @param[out] v, the random vector.
 */
static inline void stern_random_weighted_bit_vector(int n, int w, char* v) {
    int* sigma = malloc(n * sizeof(int));
    stern_random_permutation(n, sigma);
    // Warning: sigma is a permutation on {1, 2, ... n} so, in order to use it
    // on a C array, you must remove 1 to be on {0, 1, ... n-1}.
    for (size_t i = 0; i < w; i++) {
        v[sigma[i] - 1] = 1;
    }
    for (size_t i = w; i < n; i++) {
        v[sigma[i] - 1] = 0;
    }
    free(sigma);
}

/*
 * Returns a random bit vector on {0, 1}^n.
 *
 * @param[in]  n, the size of the vector.
 * @param[out] v, the random vector.
 */
static inline void stern_random_bit_vector(int n, char* v) {
    for (size_t i = 0; i < n; i++) {
        v[i] = stern_next_random_ranged_int(2);
    }
}

#define stern_seed_RNG(seed)                                             \
    seed_RNG_ULLs[STERN_CHOSEN_RNG]                                       \
        ? seed_RNG_ULLs[STERN_CHOSEN_RNG]((unsigned long long int)(seed)) \
        : (seed_RNG_ULs[STERN_CHOSEN_RNG]                                 \
            ? seed_RNG_ULs[STERN_CHOSEN_RNG]((unsigned long int)(seed))   \
            : (seed_RNG_Us[STERN_CHOSEN_RNG]                              \
                ? seed_RNG_Us[STERN_CHOSEN_RNG]((unsigned int)(seed))     \
                : seed_RNG_Ls[STERN_CHOSEN_RNG]((long int)(seed))))

#define stern_free_RNG delete_RNGs[STERN_CHOSEN_RNG]

#endif // ifndef STERN_RANDOM
