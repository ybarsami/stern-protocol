#ifndef STERN_MATRICES
#define STERN_MATRICES

#include <stdio.h>  // function  fprintf (output strings on a stream)
                    // constant  stderr (standard error output stream)
#include <stdlib.h> // functions malloc, free ((de)allocate memory)
                    //           exit (error handling)
                    // constant  EXIT_FAILURE (error handling)
                    // type      size_t

/*****************************************************************************
 *                           Matrix functions                                *
 *                                char**                                     *
 *****************************************************************************/

/*
 * Matrix allocation - char** format.
 * @param[in] nbRow is the number of rows of the matrix to be allocated.
 * @param[in] nbCol is the number of cols of the matrix to be allocated.
 * @return    a newly allocated (nbRow x nbCol) matrix.
 */
static inline char** allocate_char_matrix(int nbRow, int nbCol) {
    char** a = malloc(nbRow * sizeof(char*));
    if (!a) {
        fprintf(stderr, "allocate_char_matrix(%d, %d) : malloc error.\n", nbRow, nbCol);
        exit(EXIT_FAILURE);
    }
    for (size_t i = 0; i < nbRow; i++) {
        a[i] = malloc(nbCol * sizeof(char));
        if (!a[i]) {
            fprintf(stderr, "allocate_char_matrix(%d, %d) : malloc error.\n", nbRow, nbCol);
            exit(EXIT_FAILURE);
        }
    }
    return a;
}

/*
 * Matrix deallocation - char** format.
 * @param[in, out] a is the (nbRow x nbCol) matrix to deallocate.
 * @param[in]      nbRow is the number of rows of the matrix to be deallocated.
 * @param[in]      nbCol is the number of cols of the matrix to be deallocated.
 */
static inline void deallocate_char_matrix(char** a, int nbRow, int nbCol) {
    for (size_t i = 0; i < nbRow; i++)
        free(a[i]);
    free(a);
}

/*
 * Matrix-vector multiplication - char** format.
 * @param[in]  nbRow is the number of rows of the matrix and the size of the vector x.
 * @param[in]  nbCol is the number of cols of the matrix and the size of the vector y.
 * @param[in]  m is a nbRow * nbCol matrix.
 * @param[in]  x is a vector of size nbCol.
 * @param[out] y is a vector of size nbRow, resulting from m * x.
 */
static inline void char_matrix_vector_multiply(int nbRow, int nbCol, char** m, char* x, char* y) {
    for (size_t i = 0; i < nbRow; i++) {
        y[i] = 0;
        for (size_t j = 0; j < nbCol; j++) {
            y[i] += m[i][j] * x[j];
        }
    }
}

/*
 * Permutation of a vector - char* format.
 * @param[in]  n is the size of the permutation sigma and of the vectors x and y.
 * @param[in]  sigma is the permutation of [n].
 * @param[in]  x is a vector of size n.
 * @param[out] y is a vector of size n, resulting from sigma(x).
 */
static inline void apply_permutation(int n, unsigned int* sigma, char* x, char* y) {
    for (size_t i = 0; i < n; i++) {
        y[i] = x[sigma[i]];
    }
}

/*
 * Bitwise xor of two vectors - char* format.
 * @param[in]  n is the size of the vectors.
 * @param[in]  x, y are vectors of size n.
 * @param[out] z is a vector of size n, resulting from x xor y.
 */
static inline void apply_xor(int n, char* x, char* y, char* z) {
    for (size_t i = 0; i < n; i++) {
        z[i] = x[i]^y[i];
    }
}

#endif // ifndef STERN_MATRICES
