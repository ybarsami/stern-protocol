# For some reason, with gcc-11, there is a compilation problem.

all:
	gcc -I. ./random.c ./prover.c -o prover -lm -lgmp
	gcc -I. ./random.c ./verifier.c -o verifier -lm -lgmp

gcc7:
	gcc-7 -I. ./random.c ./prover.c -o prover -lm -lgmp
	gcc-7 -I. ./random.c ./verifier.c -o verifier -lm -lgmp

clean:
	rm prover verifier

