#ifndef STERN_PARAMETERS
#define STERN_PARAMETERS

// Default value for the security parameter.
#define DEFAULT_N 1024

// Number of iterations of the protocol we make.
#define NB_TESTS 10000

// Define SLEEP so that the verifier waits a little bit before the beginning of each iteration.
#define SLEEP

#endif // ifndef STERN_PARAMETERS

