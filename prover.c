/* A simple server in the internet domain using TCP
 * The port number is passed as an argument
 *
 * gcc random.c prover.c -o prover -lgmp
 */

#include <arpa/inet.h>   // functions htons (host-to-network short)
#include <assert.h>      // macro     assert
#include <math.h>        // functions ceil, log2 (logarithm base 2)
#include <netinet/in.h>  // type      struct sockaddr_in
#include <netdb.h>       // type      struct hostent
                         // function  gethostbyname
#include <stdint.h>      // types     int64_t, uint32_t
#include <stdio.h>       // functions fprintf, perror, sprintf
#include <gmp.h>         // WARNING: include stdio.h before gmp.h if we use functions mpz_out_raw, mpz_inp_raw
                         // type      mpz_t
                         // functions mpz_init(s), mpz_clear(s)
                         // functions mpz_add, mpz_mod, mpz_mul, mpz_cmp, mpz_sizeinbase
#include <stdlib.h>      // functions exit (error handling), atoi (convert to integer)
                         // constant  EXIT_FAILURE (error handling)
                         // type      size_t
#include <string.h>      // functions memcpy, memset
#include <sys/types.h>
#include <sys/socket.h>  // functions accept (counterpart of client's connect), socket
                         // constants AF_INET, SOCK_STREAM
#include <unistd.h>      // type      ssize_t
                         // functions close, read, write
#include "maths.h"       // functions ceiling_division, handle_mpz_import, handle_mpz_export, mersenne_prime
#include "matrices.h"    // functions allocate_char_matrix, deallocate_char_matrix
#include "parameters.h"  // constants N, K, W, NB_TESTS
#include "random.h"      // macros    stern_seed_RNG, stern_free_RNG
                         // functions stern_random_bit_matrix, stern_random_bit_vector, stern_random_weighted_bit_vector
                         //           stern_random_permutation, stern_next_random_bytes, seed_64bits

void error(const char* msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char* argv[]) {
    // Client-server connection.
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    ssize_t bytes_exchanged;
    
    // Usage of server requires port number (between 2000 and 65535).
    // Value for N is optional.
    if (argc < 2) {
        fprintf(stderr, "Usage: %s port [N]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    portno = atoi(argv[1]);
    if (portno < 2000 || portno > 65535) {
        fprintf(stderr, "ERROR: port must be between 2000 and 65535\n");
        exit(EXIT_FAILURE);
    }
    // Default value for N: see parameters.h
    int N = (argc < 3) ? DEFAULT_N : atoi(argv[2]);
    if (N <= 0) {
        fprintf(stderr, "ERROR: N must be greater than 0\n");
        exit(EXIT_FAILURE);
    }
    int K = ceiling_division(N, 2);
    int W = (int)ceil(0.11 * N);
    
    int nb_indices = 3;
    // All the computations are done thanks to the gmp library which works with the following data type:
    mpz_t a[nb_indices], b[nb_indices], z[nb_indices], y[nb_indices], q[nb_indices];
    for (size_t i = 0; i < nb_indices; i++) {
        mpz_inits(a[i], b[i], z[i], y[i], q[i], (void*)0);
    }
    // q[0], q[1], q[2] depend on the security we want to achieve --- the parameter N
    // 64 is chosen so that (2^{-64})^{1/4} << 1 (soundness loss)
    mersenne_prime(N * ceil(log2(N)) + 2 * N + 64, q[0]); // q[0] := 2^p[0] - 1
    mersenne_prime(N * ceil(log2(N)) + 2 * N + 64, q[1]); // q[1] := 2^p[1] - 1
    mersenne_prime(N * ceil(log2(N)) + 2 * N + 64, q[2]); // q[2] := 2^p[2] - 1
    size_t q_size[nb_indices]; // Size in bytes
    for (size_t i = 0; i < nb_indices; i++) {
        q_size[i] = ceiling_division(mpz_sizeinbase(q[i], 2), 8);
    }
    size_t prefix_sum_of_prime_sizes[nb_indices + 1];
    prefix_sum_of_prime_sizes[0] = 0;
    for (size_t i = 0; i < nb_indices; i++) {
        prefix_sum_of_prime_sizes[i + 1] = prefix_sum_of_prime_sizes[i] + q_size[i];
    }
    size_t sum_of_prime_sizes = prefix_sum_of_prime_sizes[nb_indices];
    // As input, Prover1, Prover2, Verifier1, and Verifier 2 all know:
    //     * a matrix H in {0, 1}^(n-k)*n
    char** H = allocate_char_matrix(N - K, N);
    stern_random_bit_matrix(N - K, N, H);
    // TODO: have only one participant compute H and send it to the others.
    //     * a column vector s in {0, 1}^(n-k) --- the syndrome
    char* s = malloc((N - K) * sizeof(char));
    // Prover1 and Prover2 share:
    //     * a column vector e in {0, 1}^n s.t. |e| = w and H*e = s
    // REMARK: here, the prover first computes a random vector e then deduces s.
    char* e = malloc(N * sizeof(char));
    stern_random_weighted_bit_vector(N, W, e);
    char_matrix_vector_multiply(N - K, N, H, e, s);
    //     * a random permutation (sigma) on [n], encoded in n*ceil(n) bits as (sigma_encoded)
    unsigned int* sigma = malloc(N * sizeof(unsigned int));
    mpz_t sigma_encoded;
    mpz_init(sigma_encoded);
    //     * a random column vector (t) on {0, 1}^n
    char* t = malloc(N * sizeof(char));
    //     * three numbers (a[i]) in Z/q[i] Z --- a[i] is stored on q_size[i] chars.
    char* a_char = malloc(sum_of_prime_sizes * sizeof(char));
    // Verifier1 and Verifier2 share:
    //     * three numbers (b[i]) in Z/q[i] Z --- b[i] is stored on q_size[i] chars.
    //       (those numbers are sent to Prover1 at each round)
    char* b_char = malloc(sum_of_prime_sizes * sizeof(char));
    // At each round, Prover1 computes:
    //     * s_prime = H * t
    char* s_prime_char = malloc((N - K) * sizeof(char));
    mpz_t s_prime;
    mpz_init(s_prime);
    //     * z[0] = s_prime + 2^(n-k) * sigma_encoded --- it is in Z/q[0] Z
    //     * z[1] = sigma(t)                          --- it is in Z/q[1] Z
    //     * z[2] = sigma(t xor e)                    --- it is in Z/q[2] Z
    char* z_char = malloc(sum_of_prime_sizes * sizeof(char));
    char* tmp_vector = malloc(N * sizeof(char));
    char* z1_vector = malloc(N * sizeof(char));
    char* z2_vector = malloc(N * sizeof(char));
    // then Prover1 commits to Verifier1 (y[i]) = (a[i]+b[i]*z[i]).
    char* y_char = malloc(sum_of_prime_sizes * sizeof(char));
    // then Verifier2 sends a challenge to Prover2.
    char challenge;
    // then Prover2 sends to Verifier2 a[i] and z[i] for i != challenge.
    char* az_char = malloc(2 * sum_of_prime_sizes * sizeof(char));
    size_t prefix_sum_of_az_sizes[2 * (nb_indices - 1) + 1];
    prefix_sum_of_az_sizes[0] = 0;
    size_t sum_of_az_sizes;
    
    // TESTING
    printf("N = %d, K = %d, W = %d, size(q[0]) = %ld, size(q[1]) = %ld, size(q[2]) = %ld.\n", N, K, W, 8*q_size[0], 8*q_size[1], 8*q_size[2]);
    
    // Open the port.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR opening socket");
    }
    memset((char*) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd,
             (struct sockaddr*) &serv_addr,
             sizeof(serv_addr)) < 0) {
        error("ERROR on binding");
    }
    
    // Wait for Verifier1 to connect.
    listen(sockfd, 5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd, 
                       (struct sockaddr*) &cli_addr, 
                       &clilen);
    if (newsockfd < 0) {
        error("ERROR on accept");
    }
    
    // Seed the random number generator.
    stern_seed_RNG(seed_64bits(1));
    
  // TESTING: NB_TESTS iterations of the protocol, to get statistics on timings.
  for (size_t i_test = 0; i_test < NB_TESTS; i_test++) {
    /*****************************************************************************
     *                               Phase 1 begin                               *
     *****************************************************************************/
    // Generate a permutation in [n] --- TODO: from shared source
    stern_random_permutation(N, sigma);
    mpz_import(sigma_encoded, N, 1, sizeof(sigma[0]), 0, 8 * sizeof(sigma[0]) - (int)ceil(log2(N)), sigma);
    // Generate a vector in {0, 1}^n --- TODO: from shared source
    stern_random_bit_vector(N, t);
    // s_prime = H * t --- in {0, 1}^(n-k)
    char_matrix_vector_multiply(N - K, N, H, t, s_prime_char);
    mpz_import(s_prime, N - K, 1, sizeof(s_prime_char[0]), 0, 8 * sizeof(s_prime_char[0]) - 1, s_prime_char);
    // z[0] = s_prime + 2^(n-k) * sigma_encoded --- it is in Z/q[0] Z
    mpz_mul_2exp(z[0], sigma_encoded, N - K); // z[0] = sigma_encoded * 2^(n-k)
    mpz_add(z[0], z[0], s_prime);             // z[0] = z[0] + s_prime
    // z[1] = sigma(t)                          --- it is in Z/q[1] Z
    apply_permutation(N, sigma, t, z1_vector);
    mpz_import(z[1], N, 1, sizeof(z1_vector[0]), 0, 8 * sizeof(z1_vector[0]) - 1, z1_vector);
    // z[2] = sigma(t xor e)                    --- it is in Z/q[2] Z
    apply_xor(N, t, e, tmp_vector);
    apply_permutation(N, sigma, tmp_vector, z2_vector);
    mpz_import(z[2], N, 1, sizeof(z2_vector[0]), 0, 8 * sizeof(z2_vector[0]) - 1, z2_vector);
    for (size_t i = 0; i < nb_indices; i++) {
        size_t offset = prefix_sum_of_prime_sizes[i];
        // Generate a random number in [0; q[i]) --- TODO: from shared source
        do {
            stern_next_random_bytes(&a_char[offset], q_size[i]);
            handle_mpz_import(a[i], &a_char[offset], q_size[i]);
            mpz_mod(a[i], a[i], q[i]);
        } while (mpz_cmp(a[i], q[i]) >= 0);
        // REMARK: a is not sent to the verifier, so there is no need to export it
        // back in its char array. Don't forget to export it in the char array if
        // it needs to be communicated (a modulo has been performed that is now not
        // reflected in the char array).
    }
    
    /*****************************************************************************
     *                     Receive the random numbers (b)                        *
     *****************************************************************************/
    // Receive the number (b) in [0; q[i]).
    bytes_exchanged = 0;
    while (bytes_exchanged != sum_of_prime_sizes) {
        ssize_t bytes_read = read(newsockfd,
                                  &b_char[bytes_exchanged],
                                  sum_of_prime_sizes - bytes_exchanged);
        if (bytes_read < 0) {
            error("ERROR reading (b) from socket");
        }
        bytes_exchanged += bytes_read;
    }
    
    for (size_t i = 0; i < nb_indices; i++) {
        size_t offset = prefix_sum_of_prime_sizes[i];
        handle_mpz_import(b[i], &b_char[offset], q_size[i]);
    
    /*****************************************************************************
     *                  Send the committed bits (y) = (a+b*z)                    *
     *****************************************************************************/
        // Prepare the message to send.
        // y = a + b * z
        mpz_mul(y[i], b[i], z[i]);
        mpz_add(y[i], y[i], a[i]);
        mpz_mod(y[i], y[i], q[i]);
        handle_mpz_export(&y_char[offset], y[i], q_size[i]);
    }
    // Send the number (y) in [0; q[i]).
    bytes_exchanged = 0;
    while (bytes_exchanged != sum_of_prime_sizes) {
        ssize_t bytes_written = write(newsockfd,
                                      &y_char[bytes_exchanged],
                                      sum_of_prime_sizes - bytes_exchanged);
        if (bytes_written < 0) {
            error("ERROR writing (y) to socket");
        }
        bytes_exchanged += bytes_written;
    }
    
    /*****************************************************************************
     *                               Phase 2 begin                               *
     *                   Receive the challenge (c) in {0, 1, 2}                  *
     *****************************************************************************/
    bytes_exchanged = 0;
    while (!bytes_exchanged) {
        ssize_t bytes_read = read(newsockfd,
                                  &challenge,
                                  1);
        if (bytes_read < 0) {
            error("ERROR reading (c) from socket");
        }
        bytes_exchanged += bytes_read;
    }
    
    /*****************************************************************************
     *                               Phase 3 begin                               *
     *****************************************************************************/
    // Computation of the positions of the numbers (a) and (z) in the buffer.
    size_t i_az = 0;
    for (size_t i = 0; i < nb_indices; i++) {
        if (i == challenge)
            continue;
        prefix_sum_of_az_sizes[i_az + 1] = prefix_sum_of_az_sizes[i_az] + q_size[i];
        i_az++;
        prefix_sum_of_az_sizes[i_az + 1] = prefix_sum_of_az_sizes[i_az] + q_size[i];
        i_az++;
    }
    size_t sum_of_az_sizes = prefix_sum_of_az_sizes[2 * (nb_indices - 1)];
    
    // Prepare the message to send
    i_az = 0;
    for (size_t i = 0; i < nb_indices; i++) {
        if (i == challenge)
            continue;
        size_t offset = prefix_sum_of_az_sizes[i_az++];
        handle_mpz_export(&az_char[offset], a[i], q_size[i]);
        offset = prefix_sum_of_az_sizes[i_az++];
        handle_mpz_export(&az_char[offset], z[i], q_size[i]);
    }
    
    // Send the numbers (a) and (z) in [0; q[i]) for i different from challenge.
    bytes_exchanged = 0;
    while (bytes_exchanged != sum_of_az_sizes) {
        ssize_t bytes_written = write(newsockfd,
                                      &az_char[bytes_exchanged],
                                      sum_of_az_sizes - bytes_exchanged);
        if (bytes_written < 0) {
            error("ERROR writing (a) and (z) to socket");
        }
        bytes_exchanged += bytes_written;
    }
  }
    
    // Be clean (-:
    close(newsockfd);
    close(sockfd);
    stern_free_RNG();
    deallocate_char_matrix(H, N - K, N);
    for (size_t i = 0; i < nb_indices; i++) {
        mpz_clears(a[i], b[i], z[i], y[i], q[i], (void*)0);
    }
    mpz_clear(sigma_encoded);
    mpz_clear(s_prime);
    free(s);
    free(e);
    free(sigma);
    free(t);
    free(s_prime_char);
    free(tmp_vector);
    free(z1_vector);
    free(z2_vector);
    free(a_char);
    free(b_char);
    free(z_char);
    free(y_char);
    return 0; 
}

