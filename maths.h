#ifndef STERN_MATHS
#define STERN_MATHS

#include <stdio.h>  // functions fprintf (output strings on a stream)
                    // constant  stderr (standard error output stream)
#include <stdlib.h> // function  exit (error handling)
                    // constant  EXIT_FAILURE (error handling)
                    // type      size_t
#include <gmp.h>    // WARNING : include stdio.h before gmp.h if we use functions mpz_out_raw, mpz_inp_raw
                    // type      mpz_t
                    // functions mpz_init, mpz_clear, mpz_init_set_str, mpz_import, mpz_export
                    // functions mpz_mul_2exp, mpz_sub

/*
 * Computes the minimum of two ints.
 *
 * @param[in] a, b.
 * @return    min(a, b).
 */
static inline size_t min(size_t a, size_t b) {
    return a < b ? a : b;
}

/*
 * Return the ceiling of a / b, as an integer (ceil function from math.h returns a double).
 *
 * @param[in] a, b
 * @return    |¯ a / b ¯|.
 */
static inline size_t ceiling_division(size_t a, size_t b) {
    // There is the risk of overflow in the obvious (a + b - 1) / b.
    return (a == 0) ? 0 : 1 + (a - 1) / b;
}

/*
 * Helper function to use the mpz_import function with standard parameters.
 *
 * @param[in]  buf, the char array containing the data to import.
 * @param[in]  count, the number of chars to read.
 * @param[out] rop, the result of the import.
 */
static inline void handle_mpz_import(mpz_t rop, char* buf, size_t count) {
    mpz_import(rop,            // output
               count,          // how many numbers are read
               1,              // among numbers: 1 for most significant number first or -1 for least significant first
               sizeof(buf[0]), // size of each number in bytes
               0,              // within each number: 1 for most significant byte first, -1 for least significant first, or 0 for the native endianness of the host CPU
               0,              // how many of the most significant bits are removed from each number
               buf);           // the data
}

/*
 * Helper function to use the mpz_export function with standard parameters.
 *
 * @param[in]  op, the number to export.
 * @param[in]  expected_count, the expected number of chars to export.
 * @param[out] buf, the result of the export.
 */
static inline void handle_mpz_export(char* buf, mpz_t op, size_t expected_count) {
    size_t countp;
    int order = 1;
    mpz_export(buf, &countp, order, sizeof(buf[0]), 0, 0, op);
    if (countp < expected_count) {
        if (order == 1) {
            int offset = expected_count - countp;
            for (size_t i = 0; i < countp; i++) {
                buf[expected_count - 1 - i] = buf[expected_count - 1 - i - offset];
            }
            for (size_t i = countp; i < expected_count; i++) {
                buf[expected_count - 1 - i] = 0;
            }
        } else {
            for (size_t i = countp; i < expected_count; i++) {
                buf[i] = 0;
            }
        }
    } else if (countp > expected_count) {
        // Problem
        fprintf(stderr, "Problem when exporting number with the GMP library. I expected to export no more than %ld bytes, and I had to export %ld bytes.\n", expected_count, countp);
        exit(EXIT_FAILURE);
    }
}

/*
 * Helper function to check a return value given by a mpz function.
 */
static inline void check_mpz_str(int ret_val) {
    if (ret_val == 0) {
        // OK
    } else {
        // Problem
        fprintf(stderr, "Problem when initializing number from string with the GMP library.\n");
        exit(EXIT_FAILURE);
    }
}

/*
 * Returns the first Mersenne prime which is at least N bits long.
 * e.g., if N > 9941 and N <= 11213, it returns the prime P = 2^11213 - 1 (this prime number takes 1402 bytes).
 * https://en.wikipedia.org/wiki/Mersenne_prime
 *
 * @param[in]  N, the number of bits required.
 * @param[out] prime, the first Mersenne prime that is at least N bits long.
 */
#define NB_POSSIBILITIES 39
static inline void mersenne_prime(int N, mpz_t prime) {
    int mersenne_p[NB_POSSIBILITIES] = { 2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607, 1279, 2203, 2281, 3217, 4253, 4423, 9689, 9941, 11213, 19937, 21701, 23209, 44497, 86243, 110503, 132049, 216091, 756839, 859433, 1257787, 1398269, 2976221, 3021377, 6972593, 13466917 };
    int p = 0;
    for (int i = 0; i < NB_POSSIBILITIES; i++) {
        if (mersenne_p[i] >= N) {
            p = mersenne_p[i];
            break;
        }
    }
    if (p == 0) {
        // Problem
        fprintf(stderr, "The Mersenne prime must be no greater than 2^%d-1.\n", mersenne_p[NB_POSSIBILITIES - 1]);
        exit(EXIT_FAILURE);
    }
    mpz_t one;
    int ret_val = mpz_init_set_str(one, "1", 10); // one = 1
    check_mpz_str(ret_val);
    mpz_mul_2exp(prime, one, p); // prime = 1 * 2^p
    mpz_sub(prime, prime, one);  // prime = prime - 1
    mpz_clear(one);
}

#endif // ifndef STERN_MATHS
